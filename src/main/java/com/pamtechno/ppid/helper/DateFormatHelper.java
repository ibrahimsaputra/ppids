package com.pamtechno.ppid.helper;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Yunus Rahmadhani
 * @date 15-Aug-20
 */
public class DateFormatHelper {
    public static final String DB_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String VIEW_FORMAT = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String TIME_FORMAT = "HH:mm:ss";

    public Timestamp getNowTimestamp(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        Timestamp timestamp = new Timestamp(date.getTime());

        return timestamp;
    }

    public String setNowTimestampFormat(String format){
        Date date = new Date();
        date.setTime(this.getNowTimestamp().getTime());
        String formattedDate = new SimpleDateFormat(format).format(date);

        return formattedDate;
    }
}
