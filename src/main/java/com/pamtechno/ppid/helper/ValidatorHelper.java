package com.pamtechno.ppid.helper;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Yunus Rahmadhani
 * @date 15-Aug-20
 */
public class ValidatorHelper {
    Map<String, String> rule = new HashMap<>();

    public Map<String, Object> validate(Map<String, Object> request){
        Map<String, Object> error = new HashMap<>();

        try {
            for(Map.Entry<String, String> mapRule: rule.entrySet()){
                Map<String, Object> subError = new HashMap<>();
                String[] splitRule = mapRule.getValue().split("\\|");

                for (String _splitRule: splitRule){
                    if (_splitRule.equals("required")){
                        if (request.get( mapRule.getKey() ) == null){
                            subError.put("required", mapRule.getKey()+" is required");
                        }
                    }
                }

                if (!subError.isEmpty()){
                    error.put(mapRule.getKey(), subError);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return error;
    }

    public ValidatorHelper check(String field, String rule){
        this.rule.put(field, rule);
        return this;
    }
}
