package com.pamtechno.ppid.helper;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;
import java.util.TimeZone;

/**
 * @author Yunus Rahmadhani
 * @date 15-Aug-20
 */
public class MapperObjectHelper {

    public Object converMapToClass(Map<String, Object> data, Class<?> castClass){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setTimeZone(TimeZone.getDefault());
        return objectMapper.convertValue(data, castClass);
    }
}
