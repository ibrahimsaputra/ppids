package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.Profil;

public interface ProfilDAO {

	List<Profil> get();

	Profil get(Long id);

	void save(Profil profil);

	void delete(Long id);
}