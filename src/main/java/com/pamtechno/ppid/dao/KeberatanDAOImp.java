package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.Keberatan;

@Repository
public class KeberatanDAOImp implements KeberatanDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Keberatan> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<Keberatan> query= currSession.createQuery("from Keberatan",Keberatan.class);
		List<Keberatan> list = query.getResultList();
		return list;
	}

	@Override
	public Keberatan get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Keberatan keberatan= currSession.get(Keberatan.class,id);
		return keberatan;
	}

	@Override
	public void save(Keberatan keberatan){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(keberatan);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Keberatan keberatan= currSession.get(Keberatan.class,id);
		currSession.delete(keberatan);
	}

}