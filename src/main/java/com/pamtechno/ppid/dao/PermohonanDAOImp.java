package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.Permohonan;

@Repository
public class PermohonanDAOImp implements PermohonanDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Permohonan> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<Permohonan> query= currSession.createQuery("from Permohonan",Permohonan.class);
		List<Permohonan> list = query.getResultList();
		return list;
	}

	@Override
	public Permohonan get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Permohonan permohonan= currSession.get(Permohonan.class,id);
		return permohonan;
	}

	@Override
	public void save(Permohonan permohonan){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(permohonan);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Permohonan permohonan= currSession.get(Permohonan.class,id);
		currSession.delete(permohonan);
	}

}