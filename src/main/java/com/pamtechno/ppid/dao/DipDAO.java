package com.pamtechno.ppid.dao;

import java.util.List;

import com.pamtechno.ppid.modal.Dip;

public interface DipDAO {

	List<Dip> get();

	Dip get(Long id);

	void save(Dip dip);

	void delete(Long id);
}