package com.pamtechno.ppid.dao;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pamtechno.ppid.modal.Menu;

@Repository
public class MenuDAOImp implements MenuDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Menu> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<Menu> query= currSession.createQuery("from Menu",Menu.class);
		List<Menu> list = query.getResultList();
		for (Menu menu:list) {
			Hibernate.initialize(menu.getInfopbs());
			Hibernate.initialize(menu.getInfopsms());
			Hibernate.initialize(menu.getInfoptsss());
			Hibernate.initialize(menu.getRegulasis());
			Hibernate.initialize(menu.getStLayanans());
		}
		
		Predicate<Menu> byParent = menu->menu.getParentMenu() == null;
		List<Menu> result =  list.stream().filter(byParent)
				.collect(Collectors.toList());
	
		return result;
	}

	@Override
	public Menu get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Menu menu= currSession.get(Menu.class,id);
		Hibernate.initialize(menu.getInfopbs());
		Hibernate.initialize(menu.getInfopsms());
		Hibernate.initialize(menu.getInfoptsss());
		Hibernate.initialize(menu.getRegulasis());
		Hibernate.initialize(menu.getStLayanans());
		return menu;
	}

	@Override
	public void save(Menu menu){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(menu);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Menu menu= currSession.get(Menu.class,id);
		currSession.delete(menu);
	}

}