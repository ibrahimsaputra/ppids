package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.Laplayanan;

@Repository
public class LaplayananDAOImp implements LaplayananDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Laplayanan> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<Laplayanan> query= currSession.createQuery("from Laplayanan",Laplayanan.class);
		List<Laplayanan> list = query.getResultList();
		return list;
	}

	@Override
	public Laplayanan get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Laplayanan laplayanan= currSession.get(Laplayanan.class,id);
		return laplayanan;
	}

	@Override
	public void save(Laplayanan laplayanan){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(laplayanan);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Laplayanan laplayanan= currSession.get(Laplayanan.class,id);
		currSession.delete(laplayanan);
	}

}