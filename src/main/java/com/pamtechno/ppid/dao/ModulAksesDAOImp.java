package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.ModulAkses;

@Repository
public class ModulAksesDAOImp implements ModulAksesDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<ModulAkses> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<ModulAkses> query= currSession.createQuery("from ModulAkses",ModulAkses.class);
		List<ModulAkses> list = query.getResultList();
		return list;
	}

//	@Override
//	public ModulAkses get(Long id){
//		Session currSession= entityManager.unwrap(Session.class);
//		ModulAkses modulAkses= currSession.get(ModulAkses.class,id);
//		return modulAkses;
//	}

	@Override
	public void save(ModulAkses modulAkses){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(modulAkses);
	}

//	@Override
//	public void delete(Long id){
//		Session currSession= entityManager.unwrap(Session.class);
//		ModulAkses modulAkses= currSession.get(ModulAkses.class,id);
//		currSession.delete(modulAkses);
//	}

}