package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.Laplayanan;

public interface LaplayananDAO {

	List<Laplayanan> get();

	Laplayanan get(Long id);

	void save(Laplayanan laplayanan);

	void delete(Long id);
}