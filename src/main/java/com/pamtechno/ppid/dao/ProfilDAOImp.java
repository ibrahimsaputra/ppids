package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.Profil;

@Repository
public class ProfilDAOImp implements ProfilDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Profil> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<Profil> query= currSession.createQuery("from Profil",Profil.class);
		List<Profil> list = query.getResultList();
		return list;
	}

	@Override
	public Profil get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Profil profil= currSession.get(Profil.class,id);
		return profil;
	}

	@Override
	public void save(Profil profil){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(profil);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Profil profil= currSession.get(Profil.class,id);
		currSession.delete(profil);
	}

}