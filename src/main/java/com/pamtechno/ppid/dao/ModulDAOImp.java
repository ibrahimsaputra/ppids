package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.Modul;

@Repository
public class ModulDAOImp implements ModulDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Modul> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<Modul> query= currSession.createQuery("from Modul",Modul.class);
		List<Modul> list = query.getResultList();
		return list;
	}

	@Override
	public Modul get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Modul modul= currSession.get(Modul.class,id);
		return modul;
	}

	@Override
	public void save(Modul modul){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(modul);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Modul modul= currSession.get(Modul.class,id);
		currSession.delete(modul);
	}

}