package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.Keberatan;

public interface KeberatanDAO {

	List<Keberatan> get();

	Keberatan get(Long id);

	void save(Keberatan keberatan);

	void delete(Long id);
}