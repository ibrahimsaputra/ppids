package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.Menu;

public interface MenuDAO {

	List<Menu> get();

	Menu get(Long id);

	void save(Menu menu);

	void delete(Long id);
}