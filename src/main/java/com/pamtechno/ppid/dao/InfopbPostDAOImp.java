package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.InfopbPost;

@Repository
public class InfopbPostDAOImp implements InfopbPostDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<InfopbPost> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<InfopbPost> query= currSession.createQuery("from InfopbPost",InfopbPost.class);
		List<InfopbPost> list = query.getResultList();
		return list;
	}

	@Override
	public InfopbPost get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		InfopbPost infopbPost= currSession.get(InfopbPost.class,id);
		return infopbPost;
	}

	@Override
	public void save(InfopbPost infopbPost){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(infopbPost);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		InfopbPost infopbPost= currSession.get(InfopbPost.class,id);
		currSession.delete(infopbPost);
	}

}