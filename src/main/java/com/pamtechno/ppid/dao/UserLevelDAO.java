package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.UserLevel;

public interface UserLevelDAO {

	List<UserLevel> get();

	UserLevel get(Long id);

	void save(UserLevel userLevel);

	void delete(Long id);
}