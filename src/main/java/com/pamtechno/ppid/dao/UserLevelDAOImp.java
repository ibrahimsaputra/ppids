package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.UserLevel;

@Repository
public class UserLevelDAOImp implements UserLevelDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<UserLevel> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<UserLevel> query= currSession.createQuery("from UserLevel",UserLevel.class);
		List<UserLevel> list = query.getResultList();
		return list;
	}

	@Override
	public UserLevel get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		UserLevel userLevel= currSession.get(UserLevel.class,id);
		return userLevel;
	}

	@Override
	public void save(UserLevel userLevel){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(userLevel);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		UserLevel userLevel= currSession.get(UserLevel.class,id);
		currSession.delete(userLevel);
	}

}