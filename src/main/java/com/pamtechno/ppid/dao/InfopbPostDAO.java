package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.InfopbPost;

public interface InfopbPostDAO {

	List<InfopbPost> get();

	InfopbPost get(Long id);

	void save(InfopbPost infopbPost);

	void delete(Long id);
}