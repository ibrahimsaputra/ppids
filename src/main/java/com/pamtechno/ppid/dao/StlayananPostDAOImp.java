package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.StlayananPost;

@Repository
public class StlayananPostDAOImp implements StlayananPostDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<StlayananPost> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<StlayananPost> query= currSession.createQuery("from StlayananPost",StlayananPost.class);
		List<StlayananPost> list = query.getResultList();
		return list;
	}

	@Override
	public StlayananPost get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		StlayananPost stlayananPost= currSession.get(StlayananPost.class,id);
		return stlayananPost;
	}

	@Override
	public void save(StlayananPost stlayananPost){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(stlayananPost);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		StlayananPost stlayananPost= currSession.get(StlayananPost.class,id);
		currSession.delete(stlayananPost);
	}

}