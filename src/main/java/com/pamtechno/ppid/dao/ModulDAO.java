package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.Modul;

public interface ModulDAO {

	List<Modul> get();

	Modul get(Long id);

	void save(Modul modul);

	void delete(Long id);
}