package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.ModulAkses;

public interface ModulAksesDAO {

	List<ModulAkses> get();

//	ModulAkses get(Long id);

	void save(ModulAkses modulAkses);

//	void delete(Long id);
}