package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.RegulasiPost;

public interface RegulasiPostDAO {

	List<RegulasiPost> get();

	RegulasiPost get(Long id);

	void save(RegulasiPost regulasiPost);

	void delete(Long id);
}