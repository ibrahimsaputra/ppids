package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.Permohonan;

public interface PermohonanDAO {

	List<Permohonan> get();

	Permohonan get(Long id);

	void save(Permohonan permohonan);

	void delete(Long id);
}