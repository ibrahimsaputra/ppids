package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.InfopsmPost;

@Repository
public class InfopsmPostDAOImp implements InfopsmPostDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<InfopsmPost> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<InfopsmPost> query= currSession.createQuery("from InfopsmPost",InfopsmPost.class);
		List<InfopsmPost> list = query.getResultList();
		return list;
	}

	@Override
	public InfopsmPost get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		InfopsmPost infopsmPost= currSession.get(InfopsmPost.class,id);
		return infopsmPost;
	}

	@Override
	public void save(InfopsmPost infopsmPost){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(infopsmPost);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		InfopsmPost infopsmPost= currSession.get(InfopsmPost.class,id);
		currSession.delete(infopsmPost);
	}

}