package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.User;

public interface UserDAO {

	List<User> get();

	User get(Long id);

	void save(User user);

	void delete(Long id);
}