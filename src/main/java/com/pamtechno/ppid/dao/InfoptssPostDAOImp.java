package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.InfoptssPost;

@Repository
public class InfoptssPostDAOImp implements InfoptssPostDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<InfoptssPost> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<InfoptssPost> query= currSession.createQuery("from InfoptssPost",InfoptssPost.class);
		List<InfoptssPost> list = query.getResultList();
		return list;
	}

	@Override
	public InfoptssPost get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		InfoptssPost infoptssPost= currSession.get(InfoptssPost.class,id);
		return infoptssPost;
	}

	@Override
	public void save(InfoptssPost infoptssPost){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(infoptssPost);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		InfoptssPost infoptssPost= currSession.get(InfoptssPost.class,id);
		currSession.delete(infoptssPost);
	}

}