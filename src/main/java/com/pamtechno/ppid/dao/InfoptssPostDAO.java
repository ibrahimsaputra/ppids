package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.InfoptssPost;

public interface InfoptssPostDAO {

	List<InfoptssPost> get();

	InfoptssPost get(Long id);

	void save(InfoptssPost infoptssPost);

	void delete(Long id);
}