package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.InfopsmPost;

public interface InfopsmPostDAO {

	List<InfopsmPost> get();

	InfopsmPost get(Long id);

	void save(InfopsmPost infopsmPost);

	void delete(Long id);
}