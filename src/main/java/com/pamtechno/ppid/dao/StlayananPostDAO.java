package com.pamtechno.ppid.dao;

import java.util.List;
import com.pamtechno.ppid.modal.StlayananPost;

public interface StlayananPostDAO {

	List<StlayananPost> get();

	StlayananPost get(Long id);

	void save(StlayananPost stlayananPost);

	void delete(Long id);
}