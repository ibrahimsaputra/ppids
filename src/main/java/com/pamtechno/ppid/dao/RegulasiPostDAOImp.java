package com.pamtechno.ppid.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.pamtechno.ppid.modal.RegulasiPost;

@Repository
public class RegulasiPostDAOImp implements RegulasiPostDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<RegulasiPost> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<RegulasiPost> query= currSession.createQuery("from RegulasiPost",RegulasiPost.class);
		List<RegulasiPost> list = query.getResultList();
		return list;
	}

	@Override
	public RegulasiPost get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		RegulasiPost regulasiPost= currSession.get(RegulasiPost.class,id);
		return regulasiPost;
	}

	@Override
	public void save(RegulasiPost regulasiPost){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(regulasiPost);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		RegulasiPost regulasiPost= currSession.get(RegulasiPost.class,id);
		currSession.delete(regulasiPost);
	}

}