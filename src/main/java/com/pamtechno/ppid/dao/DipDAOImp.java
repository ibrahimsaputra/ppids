package com.pamtechno.ppid.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pamtechno.ppid.modal.Dip;

@Repository
public class DipDAOImp implements DipDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Dip> get(){
		Session currSession = entityManager.unwrap(Session.class);
		Query<Dip> query= currSession.createQuery("from Dip",Dip.class);
		List<Dip> list = query.getResultList();
		return list;
	}

	@Override
	public Dip get(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Dip dip= currSession.get(Dip.class,id);
		return dip;
	}

	@Override
	public void save(Dip dip){
		Session currSession= entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(dip);
	}

	@Override
	public void delete(Long id){
		Session currSession= entityManager.unwrap(Session.class);
		Dip dip= currSession.get(Dip.class,id);
		currSession.delete(dip);
	}

}