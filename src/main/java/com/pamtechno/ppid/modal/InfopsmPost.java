package com.pamtechno.ppid.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="infopsm_post")
public class InfopsmPost{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	
	public Long getId_menu() {
		Long id_menu = 0L;
		try {
			menu.getId();
			id_menu = menu.getId() ;
		} catch (Exception e) {
			id_menu = 0L;
		}
		return id_menu;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_menu", referencedColumnName = "id")
	private Menu menu;

	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		menu.getInfopsms().add(this);
		this.menu = menu;
	}

	@NotNull
	@Column
	private String judul;
	public String getJudul() {
		return judul;
	}
	public void setJudul(String judul){
		this.judul = judul;
	}

	@NotNull
	@Column(columnDefinition = "text")
	private String body;
	public String getBody() {
		return body;
	}
	public void setBody(String body){
		this.body = body;
	}

	@NotNull
	@Column
	private String thumbnail;
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail){
		this.thumbnail = thumbnail;
	}

	@NotNull
	@Column
	private String link_pdf;
	public String getLink_pdf() {
		return link_pdf;
	}
	public void setLink_pdf(String link_pdf){
		this.link_pdf = link_pdf;
	}

	@CreationTimestamp
	@Column(updatable = false)
	private Date created_at;
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at){
		this.created_at = created_at;
	}

	@UpdateTimestamp
	@Column
	private Date updated_at;
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at){
		this.updated_at = updated_at;
	}

	@Column
	private Byte enabled;
	public Byte getEnabled() {
		return enabled;
	}
	public void setEnabled(Byte enabled){
		this.enabled = enabled;
	}

	@PrePersist
	public void onPrePersist(){
		 if (this.enabled == null){this.enabled = 1;}
	}

}