package com.pamtechno.ppid.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@Table(name="user")
public class User{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	
	@NonNull
	@Column
	private Long id_level;
	public Long getId_level() {
		return id_level;
	}
	public void setId_level(Long id_level){
		this.id_level = id_level;
	}

	@NonNull
	@Column
	private String username;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username){
		this.username = username;
	}

	@NonNull
	@Column
	private String password;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password){
		this.password = password;
	}
	
	@CreationTimestamp
	@Column(updatable = false)
	@JsonProperty("created_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date created_at;
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at){
		this.created_at = created_at;
	}

	@UpdateTimestamp
	@Column
	@JsonProperty("updated_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updated_at;
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at){
		this.updated_at = updated_at;
	}
	
	@Column (name = "enabled",columnDefinition = "tinyint(4) default '1'")
	private Byte enabled;
	public Byte getEnabled() {
		return enabled;
	}
	public void setEnabled(Byte enabled){
		this.enabled = enabled;
	}
	
	@PrePersist
	public void onPrePersist(){
		if (this.enabled == null) {
			this.enabled = 1;
		}
//		if (this.created_at == null) {
//			Calendar calendar = Calendar.getInstance();
//			java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
//			System.out.println("date: "+ ourJavaTimestampObject.toString());
//			this.created_at = ourJavaTimestampObject;
//		}
//		if (this.updated_at == null) {
//			Calendar calendar = Calendar.getInstance();
//			java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
//			this.updated_at = ourJavaTimestampObject;
//		}
	}
	

}