package com.pamtechno.ppid.modal;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.persistence.PrePersist;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="profil")
public class Profil{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}

	@NotNull
	@Column
	private String profil_singkat;
	public String getProfil_singkat() {
		return profil_singkat;
	}
	public void setProfil_singkat(String profil_singkat){
		this.profil_singkat = profil_singkat;
	}

	@NotNull
	@Column
	private String tugas_fungsi;
	public String getTugas_fungsi() {
		return tugas_fungsi;
	}
	public void setTugas_fungsi(String tugas_fungsi){
		this.tugas_fungsi = tugas_fungsi;
	}

	@NotNull
	@Column
	private String link_struktur;
	public String getLink_struktur() {
		return link_struktur;
	}
	public void setLink_struktur(String link_struktur){
		this.link_struktur = link_struktur;
	}

	@NotNull
	@Column
	private String visi_misi;
	public String getVisi_misi() {
		return visi_misi;
	}
	public void setVisi_misi(String visi_misi){
		this.visi_misi = visi_misi;
	}

	@CreationTimestamp
	@Column(updatable = false)
	private Date created_at;
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at){
		this.created_at = created_at;
	}

	@UpdateTimestamp
	@Column
	private Date updated_at;
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at){
		this.updated_at = updated_at;
	}

	@PrePersist
	public void onPrePersist(){
	}

}