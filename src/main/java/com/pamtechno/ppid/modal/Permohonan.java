package com.pamtechno.ppid.modal;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.persistence.PrePersist;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="permohonan")
public class Permohonan{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}

	@NotNull
	@Column
	private String nama_pemohon;
	public String getNama_pemohon() {
		return nama_pemohon;
	}
	public void setNama_pemohon(String nama_pemohon){
		this.nama_pemohon = nama_pemohon;
	}

	@NotNull
	@Column
	private String email;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email){
		this.email = email;
	}

	@NotNull
	@Column
	private String noktp;
	public String getNoktp() {
		return noktp;
	}
	public void setNoktp(String noktp){
		this.noktp = noktp;
	}

	@NotNull
	@Column
	private String alamat;
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat){
		this.alamat = alamat;
	}

	@NotNull
	@Column
	private String nohp;
	public String getNohp() {
		return nohp;
	}
	public void setNohp(String nohp){
		this.nohp = nohp;
	}

	@NotNull
	@Column
	private String tujuan;
	public String getTujuan() {
		return tujuan;
	}
	public void setTujuan(String tujuan){
		this.tujuan = tujuan;
	}

	@NotNull
	@Column
	private String tipe_pemohon;
	public String getTipe_pemohon() {
		return tipe_pemohon;
	}
	public void setTipe_pemohon(String tipe_pemohon){
		this.tipe_pemohon = tipe_pemohon;
	}

	@NotNull
	@Column
	private String detail_wni;
	public String getDetail_wni() {
		return detail_wni;
	}
	public void setDetail_wni(String detail_wni){
		this.detail_wni = detail_wni;
	}

	@NotNull
	@Column
	private String upload_ktp;
	public String getUpload_ktp() {
		return upload_ktp;
	}
	public void setUpload_ktp(String upload_ktp){
		this.upload_ktp = upload_ktp;
	}

	@NotNull
	@Column
	private String info_kategori;
	public String getInfo_kategori() {
		return info_kategori;
	}
	public void setInfo_kategori(String info_kategori){
		this.info_kategori = info_kategori;
	}

	@NotNull
	@Column
	private String info_diminta;
	public String getInfo_diminta() {
		return info_diminta;
	}
	public void setInfo_diminta(String info_diminta){
		this.info_diminta = info_diminta;
	}

	@NotNull
	@Column
	private String cara;
	public String getCara() {
		return cara;
	}
	public void setCara(String cara){
		this.cara = cara;
	}

	@NotNull
	@Column
	private String bentuk;
	public String getBentuk() {
		return bentuk;
	}
	public void setBentuk(String bentuk){
		this.bentuk = bentuk;
	}

	@NotNull
	@Column
	private String alasan;
	public String getAlasan() {
		return alasan;
	}
	public void setAlasan(String alasan){
		this.alasan = alasan;
	}

	@NotNull
	@Column
	private String kegunaan;
	public String getKegunaan() {
		return kegunaan;
	}
	public void setKegunaan(String kegunaan){
		this.kegunaan = kegunaan;
	}

	@NotNull
	@Column
	private String status;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status){
		this.status = status;
	}

	@CreationTimestamp
	@Column(updatable = false)
	private Date created_at;
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at){
		this.created_at = created_at;
	}

	@UpdateTimestamp
	@Column
	private Date updated_at;
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at){
		this.updated_at = updated_at;
	}

	@PrePersist
	public void onPrePersist(){
		 if (this.status == null || this.status.isEmpty() ){this.status = "belum dibaca";}
	}

}