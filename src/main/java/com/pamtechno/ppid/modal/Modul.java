package com.pamtechno.ppid.modal;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name="modul")
public class Modul{
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	
	@OneToMany(mappedBy = "userLevel" , fetch = FetchType.LAZY )
	private Set<ModulAkses> modulAksess = new HashSet<ModulAkses>();
	
	
	public Set<ModulAkses> getModulAksess() {
		return modulAksess;
	}
	public void setModulAksess(Set<ModulAkses> modulAksess) {
		for (ModulAkses modulAkses : modulAksess) {
			modulAkses.setModul(this);
		}
		this.modulAksess = modulAksess;
	}

	@NotNull
	@Column
	private String kode;
	public String getKode() {
		return kode;
	}
	public void setKode(String kode){
		this.kode = kode;
	}

	@NotNull
	@Column
	private String nama;
	public String getNama() {
		return nama;
	}
	public void setNama(String nama){
		this.nama = nama;
	}

	@CreationTimestamp
	@Column(updatable = false)
	private Timestamp created_at;
	public Timestamp getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Timestamp created_at){
		this.created_at = created_at;
	}

	@Column
	private Byte enabled;
	public Byte getEnabled() {
		return enabled;
	}
	public void setEnabled(Byte enabled){
		this.enabled = enabled;
	}

	@PrePersist
	public void onPrePersist(){
	}

}