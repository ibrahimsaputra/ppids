package com.pamtechno.ppid.modal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ModulAksesPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="id_level")
	private Long id_level;
	
	@Column(name="id_modul")
	private Long id_modul;

	public Long getId_level() {
		return id_level;
	}

	public void setId_level(Long id_level) {
		this.id_level = id_level;
	}

	public Long getId_modul() {
		return id_modul;
	}

	public void setId_modul(Long id_modul) {
		this.id_modul = id_modul;
	}

}