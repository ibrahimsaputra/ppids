package com.pamtechno.ppid.modal;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="menu")
public class Menu{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	
	@OneToMany(
			mappedBy="parentMenu",
			cascade = CascadeType.PERSIST,
			fetch = FetchType.EAGER)
	private Set<Menu> subMenus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_parent")
	
	private Menu parentMenu;
		
	public Long getId_parent() {
		Long id_parent = 0L;
		try {
			parentMenu.getId();
			id_parent = parentMenu.getId() ;
		} catch (Exception e) {
			id_parent = 0L;
		}
		return id_parent;
	}

	@NotNull
	@Column
	private String nama_menu;
	public String getNama_menu() {
		return nama_menu;
	}
	public void setNama_menu(String nama_menu){
		this.nama_menu = nama_menu;
	}

	@NotNull
	@Column
	private String tipe;
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe){
		this.tipe = tipe;
	}

	@Column
	private Byte enabled;
	public Byte getEnabled() {
		return enabled;
	}
	public void setEnabled(Byte enabled){
		this.enabled = enabled;
	}

	@CreationTimestamp
	@Column(updatable = false)
	private Date created_at;
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at){
		this.created_at = created_at;
	}

	@UpdateTimestamp
	@Column
	private Date updated_at;
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at){
		this.updated_at = updated_at;
	}

	@PrePersist
	public void onPrePersist(){
		 if (this.enabled == null){this.enabled = 1;}
	}
	
	public Menu() {
		subMenus = new HashSet<>();
		infopbs = new HashSet<>();
		infopsms = new HashSet<>();
		infoptsss = new HashSet<>();
		regulasis = new HashSet<>();
		stLayanans = new HashSet<>();
	}
	
	public Menu(Menu parentMenu, String nama_menu) {
		this.parentMenu = parentMenu;
		this.nama_menu = nama_menu;
		subMenus = new HashSet<>();
		infopbs = new HashSet<>();
		infopsms = new HashSet<>();
		infoptsss = new HashSet<>();
		regulasis = new HashSet<>();
		stLayanans = new HashSet<>();
	}
	@OneToMany(
			mappedBy = "menu",
			cascade = CascadeType.PERSIST,
			fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<InfopbPost> infopbs;
	
	@OneToMany(
			mappedBy = "menu",
			cascade = CascadeType.PERSIST,
			fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<InfopsmPost> infopsms;
	
	@OneToMany(
			mappedBy = "menu",
			cascade = CascadeType.PERSIST,
			fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<InfoptssPost> infoptsss;
	
	@OneToMany(
			mappedBy = "menu",
			cascade = CascadeType.PERSIST,
			fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<RegulasiPost> regulasis;

	@OneToMany(
			mappedBy = "menu",
			cascade = CascadeType.PERSIST,
			fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<StlayananPost> stLayanans;
	
	public Set<Menu> getSubMenus() {
		return subMenus;
	}
	public void setSubMenus(Set<Menu> subMenus) {
		for(Menu subMenu: subMenus) {
			subMenu.setParentMenu(this);
		}
		this.subMenus = subMenus;
	}
	
	
	public Menu getParentMenu() {
		return parentMenu;
	}
	@JsonIgnore
	public void setParentMenu(Menu parentMenu) {
		parentMenu.getSubMenus().add(this);
		this.parentMenu = parentMenu;
	}
	public Set<InfopbPost> getInfopbs() {
		return infopbs;
	}
	public void setInfopbs(Set<InfopbPost> infopbs) {
		for(InfopbPost infopb: infopbs) {
			infopb.setMenu(this);
		}
		this.infopbs = infopbs;
	}
	public Set<InfopsmPost> getInfopsms() {
		for(InfopsmPost infopsm: infopsms) {
			infopsm.setMenu(this);
		}
		return infopsms;
	}
	public void setInfopsms(Set<InfopsmPost> infopsm) {
		this.infopsms = infopsm;
	}
	public Set<InfoptssPost> getInfoptsss() {
		
		return infoptsss;
	}
	public void setInfoptsss(Set<InfoptssPost> infoptsss) {
		for(InfoptssPost infoptss: infoptsss) {
			infoptss.setMenu(this);
		}
		this.infoptsss = infoptsss;
	}
	public Set<RegulasiPost> getRegulasis() {
		return regulasis;
	}
	public void setRegulasis(Set<RegulasiPost> regulasis) {
		for(RegulasiPost regulasi: regulasis) {
			regulasi.setMenu(this);
		}
		this.regulasis = regulasis;
	}
	public Set<StlayananPost> getStLayanans() {
		return stLayanans;
	}
	public void setStLayanans(Set<StlayananPost> stLayanans) {
		for(StlayananPost stLayanan: stLayanans) {
			stLayanan.setMenu(this);
		}
		this.stLayanans = stLayanans;
	}
	
	

}