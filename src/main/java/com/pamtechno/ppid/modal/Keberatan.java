package com.pamtechno.ppid.modal;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.persistence.PrePersist;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="keberatan")
public class Keberatan{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}

	@NotNull
	@Column
	private String nama_pemohon;
	public String getNama_pemohon() {
		return nama_pemohon;
	}
	public void setNama_pemohon(String nama_pemohon){
		this.nama_pemohon = nama_pemohon;
	}

	@NotNull
	@Column
	private String email;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email){
		this.email = email;
	}

	@NotNull
	@Column
	private String alamat;
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat){
		this.alamat = alamat;
	}

	@NotNull
	@Column
	private String nohp;
	public String getNohp() {
		return nohp;
	}
	public void setNohp(String nohp){
		this.nohp = nohp;
	}

	@NotNull
	@Column
	private String pil_keberatan;
	public String getPil_keberatan() {
		return pil_keberatan;
	}
	public void setPil_keberatan(String pil_keberatan){
		this.pil_keberatan = pil_keberatan;
	}

	@NotNull
	@Column
	private String als_keberatan;
	public String getAls_keberatan() {
		return als_keberatan;
	}
	public void setAls_keberatan(String als_keberatan){
		this.als_keberatan = als_keberatan;
	}

	@NotNull
	@Column
	private String als_lain;
	public String getAls_lain() {
		return als_lain;
	}
	public void setAls_lain(String als_lain){
		this.als_lain = als_lain;
	}

	@NotNull
	@Column
	private String status;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status){
		this.status = status;
	}

	@CreationTimestamp
	@Column(updatable = false)
	private Date created_at;
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at){
		this.created_at = created_at;
	}

	@UpdateTimestamp
	@Column
	private Date updated_at;
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at){
		this.updated_at = updated_at;
	}

	@Column
	private Byte enabled;
	public Byte getEnabled() {
		return enabled;
	}
	public void setEnabled(Byte enabled){
		this.enabled = enabled;
	}

	@PrePersist
	public void onPrePersist(){
		 if (this.status == null || this.status.isEmpty()){this.status = "belum dibaca";}
		 if (this.enabled == null){this.enabled = 1;}
	}

}