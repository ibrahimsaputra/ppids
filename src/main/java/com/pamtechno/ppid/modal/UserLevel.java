package com.pamtechno.ppid.modal;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="user_level")
public class UserLevel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	
	@OneToMany(mappedBy = "modul")
	private Set<ModulAkses> modulAksess = new HashSet<ModulAkses>();
	
	public Set<ModulAkses> getModulAksess() {
		return modulAksess;
	}
	public void setModulAksess(Set<ModulAkses> modulAksess) {
		for(ModulAkses modulAkses : modulAksess) {
			modulAkses.setUserLevel(this);
		}
		this.modulAksess = modulAksess;
	}

	@NotNull
	@Column
	private String level_name;
	public String getLevel_name() {
		return level_name;
	}
	public void setLevel_name(String level_name){
		this.level_name = level_name;
	}

	@CreationTimestamp
	@Column(updatable = false)
	private Timestamp created_at;
	public Timestamp getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Timestamp created_at){
		this.created_at = created_at;
	}

	@UpdateTimestamp
	@Column
	private Timestamp updated_at;
	public Timestamp getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Timestamp updated_at){
		this.updated_at = updated_at;
	}

	@PrePersist
	public void onPrePersist(){
	}

}