package com.pamtechno.ppid.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="dip")
public class Dip{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "bigint(20)")
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}

	@NotNull
	@Column
	private String rincian;
	
	@NotNull
	@Column
	private String pejabat;    
	
	public String getRincian() {
		return rincian;
	}
	public void setRincian(String rincian) {
		this.rincian = rincian;
	}
	public String getPejabat() {
		return pejabat;
	}
	public void setPejabat(String pejabat) {
		this.pejabat = pejabat;
	}
	public String getPngjwb() {
		return pngjwb;
	}
	public void setPngjwb(String pngjwb) {
		this.pngjwb = pngjwb;
	}
	public String getWktbuatinfo() {
		return wktbuatinfo;
	}
	public void setWktbuatinfo(String wktbuatinfo) {
		this.wktbuatinfo = wktbuatinfo;
	}
	public String getTmptbuatinfo() {
		return tmptbuatinfo;
	}
	public void setTmptbuatinfo(String tmptbuatinfo) {
		this.tmptbuatinfo = tmptbuatinfo;
	}
	public Byte getBfi_soft() {
		return bfi_soft;
	}
	public void setBfi_soft(Byte bfi_soft) {
		this.bfi_soft = bfi_soft;
	}
	public Byte getBfi_hard() {
		return bfi_hard;
	}
	public void setBfi_hard(Byte bfi_hard) {
		this.bfi_hard = bfi_hard;
	}
	public Byte getJi_bk() {
		return ji_bk;
	}
	public void setJi_bk(Byte ji_bk) {
		this.ji_bk = ji_bk;
	}
	public Byte getJi_ts() {
		return ji_ts;
	}
	public void setJi_ts(Byte ji_ts) {
		this.ji_ts = ji_ts;
	}
	public Byte getJi_sm() {
		return ji_sm;
	}
	public void setJi_sm(Byte ji_sm) {
		this.ji_sm = ji_sm;
	}
	public String getJk_wkt_simpan() {
		return jk_wkt_simpan;
	}
	public void setJk_wkt_simpan(String jk_wkt_simpan) {
		this.jk_wkt_simpan = jk_wkt_simpan;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	@NotNull
	@Column
	private String pngjwb;
	
	@NotNull
	@Column
	private String wktbuatinfo;
	
	@NotNull
	@Column
	private String tmptbuatinfo;
	
	@NotNull
	@Column
	private Byte bfi_soft;
	
	@NotNull
	@Column
	private Byte bfi_hard;
	
	@NotNull
	@Column
	private Byte ji_bk;
	
	@NotNull
	@Column
	private Byte ji_ts;
	
	@NotNull
	@Column
	private Byte ji_sm;
	
	@NotNull
	@Column
	private String jk_wkt_simpan;
	
	@NotNull
	@Column
	private String keterangan;
	
	@CreationTimestamp
	@Column(updatable = false)
	private Date created_at;
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at){
		this.created_at = created_at;
	}

	@UpdateTimestamp
	@Column
	private Date updated_at;
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at){
		this.updated_at = updated_at;
	}

	@Column
	private Byte enabled;
	public Byte getEnabled() {
		return enabled;
	}
	public void setEnabled(Byte enabled){
		this.enabled = enabled;
	}

	@PrePersist
	public void onPrePersist(){
		 if (this.enabled == null){this.enabled = 1;}
	}

}