package com.pamtechno.ppid.modal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="modul_akses")
public class ModulAkses{
	
	@EmbeddedId
	private ModulAksesPK id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("id_level")//this is the name of attr of ModulAksesPK.class
	@JoinColumn(name="id_level")
	private UserLevel userLevel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("id_level")//this is the name of attr of ModulAksesPK.class
	@JoinColumn(name="id_level")
	private Modul modul;
	

	public ModulAksesPK getId() {
		return id;
	}
	public void setId(ModulAksesPK id) {
		this.id = id;
	}
	public UserLevel getUserLevel() {
		return userLevel;
	}
	public void setUserLevel(UserLevel userLevel) {
		userLevel.getModulAksess().add(this);
		this.userLevel = userLevel;
	}
	public Modul getModul() {
		return modul;
	}
	public void setModul(Modul modul) {
		modul.getModulAksess().add(this);
		this.modul = modul;
	}

	@NotNull
	@Column
	private Boolean l;
	public Boolean getL() {
		return l;
	}
	public void setL(Boolean l){
		this.l = l;
	}

	@NotNull
	@Column
	private Boolean d;
	public Boolean getD() {
		return d;
	}
	public void setD(Boolean d){
		this.d = d;
	}

	@NotNull
	@Column
	private Boolean t;
	public Boolean getT() {
		return t;
	}
	public void setT(Boolean t){
		this.t = t;
	}

	@NotNull
	@Column
	private Boolean u;
	public Boolean getU() {
		return u;
	}
	public void setU(Boolean u){
		this.u = u;
	}

	@NotNull
	@Column
	private Boolean h;
	public Boolean getH() {
		return h;
	}
	public void setH(Boolean h){
		this.h = h;
	}

	@PrePersist
	public void onPrePersist(){
	}

}