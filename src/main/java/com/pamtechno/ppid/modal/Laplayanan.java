package com.pamtechno.ppid.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="laplayanan")
public class Laplayanan{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}

	@NotNull
	@Column
	private String tahun;
	public String getTahun() {
		return tahun;
	}
	public void setTahun(String tahun){
		this.tahun = tahun;
	}

	@NotNull
	@Column
	private String link;
	public String getLink() {
		return link;
	}
	public void setLink(String link){
		this.link = link;
	}

	@NotNull
	@Column
	private Byte enabled;
	public Byte getEnabled() {
		return enabled;
	}
	public void setEnabled(Byte enabled){
		this.enabled = enabled;
	}

	@CreationTimestamp
	@Column(updatable = false)
	private Date created_at;
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at){
		this.created_at = created_at;
	}

	@UpdateTimestamp
	@Column
	private Date updated_at;
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at){
		this.updated_at = updated_at;
	}

	@PrePersist
	public void onPrePersist(){
		 if (this.enabled == null){this.enabled = 1;}
	}

}