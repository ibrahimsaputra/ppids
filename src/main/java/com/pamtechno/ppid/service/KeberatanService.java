package com.pamtechno.ppid.service;

import java.util.List;
import java.util.Map;

import com.pamtechno.ppid.modal.Keberatan;

public interface KeberatanService {

	List<Map<String, Object>> get();

	Map<String, Object> get(String id);

	String save(Map<String, Object> keberatan);

	String update(Map<String, Object> keberatan, String id);

	void delete(String id);
}