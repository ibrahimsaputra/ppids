package com.pamtechno.ppid.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pamtechno.ppid.helper.DBHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pamtechno.ppid.modal.Laplayanan;

import com.pamtechno.ppid.dao.LaplayananDAO;

@Service
public class LaplayananServiceImp implements LaplayananService {

	@Autowired
	private DBHelper dbHelper;

	private Logger log = Logger.getLogger(getClass());
	private final String TABLE_LAPLAYANAN = "laplayanan";

	@Override
	public List<Map<String, Object>> get(){
		List<Map<String, Object>> response = new ArrayList<>();
		try{
			response = dbHelper.table(TABLE_LAPLAYANAN)
					.get();

			return response.isEmpty()?null:response;
		}catch (Exception e){
			return null;
		}
	}

	@Transactional
	@Override
	public Map<String, Object> get(Long id){
		Map<String, Object> response = new HashMap<>();
		try{
			response = dbHelper.table(TABLE_LAPLAYANAN)
					.where("id", id.toString())
					.first();

			return response.isEmpty()?null:response;
		}catch (Exception e){
			return null;
		}
	}

	@Transactional
	@Override
	public void save(Map<String, Object> dip){
		try{
			Boolean status = dbHelper.table(TABLE_LAPLAYANAN)
					.insert(dip);

		}catch (Exception e){
		}
	}

	@Override
	public void update(Map<String, Object> dip, String id) {
		try{
			Boolean status = dbHelper.table(TABLE_LAPLAYANAN)
					.where("id", id)
					.update(dip);
		}catch (Exception e){
		}
	}

	@Transactional
	@Override
	public void delete(String id){
		try{
			Boolean status = dbHelper.table(TABLE_LAPLAYANAN)
					.where("id", id)
					.delete();
		}catch (Exception e){
		}
	}

}