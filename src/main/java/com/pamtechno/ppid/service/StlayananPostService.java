package com.pamtechno.ppid.service;

import java.util.List;
import com.pamtechno.ppid.modal.StlayananPost;

public interface StlayananPostService {

	List<StlayananPost> get();

	StlayananPost get(Long id);

	void save(StlayananPost stlayananPost,Long id_menu);

	void delete(Long id);
}