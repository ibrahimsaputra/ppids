package com.pamtechno.ppid.service;

import java.util.List;
import com.pamtechno.ppid.modal.User;

public interface UserService {

	List<User> get();

	User get(Long id);

	void save(User user);

	void delete(Long id);
}