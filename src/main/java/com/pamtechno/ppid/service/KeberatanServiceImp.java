package com.pamtechno.ppid.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pamtechno.ppid.helper.DBHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pamtechno.ppid.modal.Keberatan;

import com.pamtechno.ppid.dao.KeberatanDAO;

@Service
public class KeberatanServiceImp implements KeberatanService {

	@Autowired
	private KeberatanDAO keberatanDao;

	@Autowired
	private DBHelper dbHelper;
	private final String TABLE_KEBERATAN = "keberatan";

	@Transactional
	@Override
	public List<Map<String, Object>> get(){
		List<Map<String, Object>> response = new ArrayList<>();
		try{
			response = dbHelper.table(TABLE_KEBERATAN)
					.get();

			return response.isEmpty()?null:response;
		}catch (Exception e){
			return null;
		}
	}

	@Transactional
	@Override
	public Map<String, Object> get(String id){
		Map<String, Object> response = new HashMap<>();
		try{
			response = dbHelper.table(TABLE_KEBERATAN)
					.where("id", id)
					.first();

			return response.isEmpty()?null:response;
		}catch (Exception e){
			return null;
		}
	}

	@Transactional
	@Override
	public String save(Map<String, Object> request){
		try{
			Boolean status = dbHelper.table(TABLE_KEBERATAN)
					.insert(request);

		}catch (Exception e){
		}

		return "";
	}

	@Override
	public String update(Map<String, Object> request, String id) {
		try{
			Boolean status = dbHelper.table(TABLE_KEBERATAN)
					.where("id", id)
					.update(request);

		}catch (Exception e){
		}

		return "";
	}

	@Transactional
	@Override
	public void delete(String id){
		try{
			Boolean status = dbHelper.table(TABLE_KEBERATAN)
					.where("id", id)
					.delete();
		}catch (Exception e){
		}
	}

}