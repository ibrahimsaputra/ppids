package com.pamtechno.ppid.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pamtechno.ppid.modal.Permohonan;

import com.pamtechno.ppid.dao.PermohonanDAO;

@Service
public class PermohonanServiceImp implements PermohonanService {

	@Autowired
	private PermohonanDAO permohonanDao;
	@Autowired
	private FileStorageService storageService;

	@Transactional
	@Override
	public List<Permohonan> get(){
		return permohonanDao.get();
	}

	@Transactional
	@Override
	public Permohonan get(Long id){
		return permohonanDao.get(id);
	}

	@Transactional
	@Override
	public void save(Permohonan permohonan){
		if (permohonan.getId() != null && permohonan.getId().equals(0L)) {
			Permohonan oldPost = permohonanDao.get(permohonan.getId());
			//Delete oldFile if new one exist
			if (permohonan.getUpload_ktp() != null && !permohonan.getUpload_ktp().isEmpty()){
				String linkUrl = oldPost.getUpload_ktp();
				String[] linkUrls = linkUrl.split("/");
			    storageService.deleteIfExist(linkUrls[linkUrls.length-1]);
			}
		}
		permohonanDao.save(permohonan);
	}

	@Transactional
	@Override
	public void delete(Long id){
		permohonanDao.delete(id);
	}

}