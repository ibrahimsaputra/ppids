package com.pamtechno.ppid.service;

import java.util.HashSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pamtechno.ppid.modal.Menu;

import com.pamtechno.ppid.dao.MenuDAO;

@Service
public class MenuServiceImp implements MenuService {

	@Autowired
	private MenuDAO menuDao;

	@Transactional
	@Override
	public List<Menu> get(){
		return menuDao.get();
	}

	@Transactional
	@Override
	public Menu get(Long id){
		return menuDao.get(id);
	}

	@Transactional
	@Override
	public void save(Menu menu, Long id_parent){
		Menu parentMenu = menuDao.get(id_parent);
		if(parentMenu != null) {
			parentMenu.getSubMenus().add(menu);
			menu.setParentMenu(parentMenu);	
		}else {
			parentMenu= new Menu();
		}
		menu.setSubMenus(new HashSet<Menu>());
		menuDao.save(menu);
	}

	@Transactional
	@Override
	public void delete(Long id){
		menuDao.delete(id);
	}

}