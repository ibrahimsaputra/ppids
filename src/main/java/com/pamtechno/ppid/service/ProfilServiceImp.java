package com.pamtechno.ppid.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pamtechno.ppid.modal.Profil;

import com.pamtechno.ppid.dao.ProfilDAO;

@Service
public class ProfilServiceImp implements ProfilService {

	@Autowired
	private ProfilDAO profilDao;
	@Autowired
	private FileStorageService storageService;
	
	@Transactional
	@Override
	public List<Profil> get(){
		return profilDao.get();
	}

	@Transactional
	@Override
	public Profil get(Long id){
		return profilDao.get(id);
	}

	@Transactional
	@Override
	public void save(Profil profil){
		if (profil.getId() != null && profil.getId().equals(0L)) {
			Profil oldPost = profilDao.get(profil.getId());
			//Delete oldFile if new one exist
			if (profil.getLink_struktur() != null && !profil.getLink_struktur().isEmpty()){
				String linkUrl = oldPost.getLink_struktur();
				String[] linkUrls = linkUrl.split("/");
			    storageService.deleteIfExist(linkUrls[linkUrls.length-1]);
			}
		}
		profilDao.save(profil);
	}

	@Transactional
	@Override
	public void delete(Long id){
		profilDao.delete(id);
	}

}