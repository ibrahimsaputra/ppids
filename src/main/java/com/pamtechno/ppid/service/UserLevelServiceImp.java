package com.pamtechno.ppid.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pamtechno.ppid.modal.UserLevel;

import com.pamtechno.ppid.dao.UserLevelDAO;

@Service
public class UserLevelServiceImp implements UserLevelService {

	@Autowired
	private UserLevelDAO userLevelDao;

	@Transactional
	@Override
	public List<UserLevel> get(){
		return userLevelDao.get();
	}

	@Transactional
	@Override
	public UserLevel get(Long id){
		return userLevelDao.get(id);
	}

	@Transactional
	@Override
	public void save(UserLevel userLevel){
		userLevelDao.save(userLevel);
	}

	@Transactional
	@Override
	public void delete(Long id){
		userLevelDao.delete(id);
	}

}