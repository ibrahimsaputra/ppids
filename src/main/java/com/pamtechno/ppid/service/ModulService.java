package com.pamtechno.ppid.service;

import java.util.List;
import com.pamtechno.ppid.modal.Modul;

public interface ModulService {

	List<Modul> get();

	Modul get(Long id);

	void save(Modul modul);

	void delete(Long id);
}