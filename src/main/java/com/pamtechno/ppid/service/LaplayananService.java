package com.pamtechno.ppid.service;

import java.util.List;
import java.util.Map;

import com.pamtechno.ppid.modal.Laplayanan;

public interface LaplayananService {

	List<Map<String, Object>> get();

	Map<String, Object> get(Long id);

	void save(Map<String, Object> dip);

	void update(Map<String, Object> dip, String id);

	void delete(String id);
}