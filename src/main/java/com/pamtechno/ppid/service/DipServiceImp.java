package com.pamtechno.ppid.service;

import com.pamtechno.ppid.helper.DBHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DipServiceImp implements DipService {

	@Autowired
	private DBHelper dbHelper;

	private Logger log = Logger.getLogger(getClass());
	@Override
	public List<Map<String, Object>> get(){
		List<Map<String, Object>> response = new ArrayList<>();
		try{
			response = dbHelper.table("dip")
					.get();

			return response.isEmpty()?null:response;
		}catch (Exception e){
			return null;
		}
	}

	@Transactional
	@Override
	public Map<String, Object> get(Long id){
		Map<String, Object> response = new HashMap<>();
		try{
			response = dbHelper.table("dip")
					.where("id", id.toString())
					.first();

			return response.isEmpty()?null:response;
		}catch (Exception e){
			return null;
		}
	}

	@Transactional
	@Override
	public void save(Map<String, Object> dip){
		try{
			Boolean status = dbHelper.table("dip")
					.insert(dip);

		}catch (Exception e){
		}
	}

	@Override
	public void update(Map<String, Object> dip, String id) {
		try{
			Boolean status = dbHelper.table("dip")
					.where("id", id)
					.update(dip);
		}catch (Exception e){
		}
	}

	@Transactional
	@Override
	public void delete(String id){
		try{
			Boolean status = dbHelper.table("dip")
					.where("id", id)
					.delete();
		}catch (Exception e){
		}
	}

}