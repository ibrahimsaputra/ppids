package com.pamtechno.ppid.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pamtechno.ppid.modal.User;

import com.pamtechno.ppid.dao.UserDAO;

@Service
public class UserServiceImp implements UserService {

	@Autowired
	private UserDAO userDao;

	@Transactional
	@Override
	public List<User> get(){
		return userDao.get();
	}

	@Transactional
	@Override
	public User get(Long id){
		return userDao.get(id);
	}

	@Transactional
	@Override
	public void save(User user){
		userDao.save(user);
	}

	@Transactional
	@Override
	public void delete(Long id){
		userDao.delete(id);
	}

}