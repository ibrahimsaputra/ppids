package com.pamtechno.ppid.service;

import java.util.List;
import com.pamtechno.ppid.modal.UserLevel;

public interface UserLevelService {

	List<UserLevel> get();

	UserLevel get(Long id);

	void save(UserLevel userLevel);

	void delete(Long id);
}