package com.pamtechno.ppid.service;

import com.pamtechno.ppid.helper.DBHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InfopbPostServiceImp implements InfopbPostService {

	@Autowired
	private DBHelper dbHelper;

	private Logger log = Logger.getLogger(getClass());

	private final String TABLE_INFOPB_POST = "infopb_post";
	private final String TABLE_MENU = "menu";

	@Transactional
	@Override
	public List<Map<String, Object>> get(){
		List<Map<String, Object>> response = new ArrayList<>();
		try{
			response = dbHelper.table(TABLE_INFOPB_POST)
					.get();

			return response.isEmpty()?null:response;
		}catch (Exception e){
			return null;
		}
	}

	@Transactional
	@Override
	public Map<String, Object> get(String id){
		Map<String, Object> response = new HashMap<>();
		try{
			response = dbHelper.table(TABLE_INFOPB_POST)
					.where("id", id)
					.first();

			return response.isEmpty()?null:response;
		}catch (Exception e){
			return null;
		}
	}

	@Transactional
	@Override
	public List<Map<String, Object>> getByIdMenu(String id_menu){
		List<Map<String, Object>> response = new ArrayList<>();
		try{
			response = dbHelper.table(TABLE_INFOPB_POST)
					.where("id_menu", id_menu)
					.get();

			return response.isEmpty()?null:response;
		}catch (Exception e){
			return null;
		}
	}

	@Transactional
	@Override
	public String save(Map<String, Object> request ,String id_menu){
		try{
			Integer menu = dbHelper.table(TABLE_MENU)
					.where("id", id_menu)
					.count();

			if (menu > 0){
				request.put("id_menu", id_menu);

				Boolean status = dbHelper.table(TABLE_INFOPB_POST)
						.insert(request);
				return "";
			}else{
				return "Menu tidak terdaftar";
			}
		}catch (Exception e){
			return "";
		}
	}

	@Override
	public String update(Map<String, Object> request, String id_menu, String id) {
		try{
			Integer menu = dbHelper.table(TABLE_MENU)
					.where("id", id_menu)
					.count();

			if (menu > 0){
				request.put("id_menu", id_menu);

				Boolean status = dbHelper.table(TABLE_INFOPB_POST)
						.where("id", id)
						.update(request);

				return "";
			}else {
				return "Menu tidak terdaftar";
			}
		}catch (Exception e){
			return "";
		}
	}

	@Transactional
	@Override
	public void delete(String id){
		try{
			Boolean status = dbHelper.table(TABLE_INFOPB_POST)
					.where("id", id)
					.delete();
		}catch (Exception e){
		}
	}

}