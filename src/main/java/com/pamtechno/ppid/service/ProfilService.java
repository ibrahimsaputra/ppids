package com.pamtechno.ppid.service;

import java.util.List;
import com.pamtechno.ppid.modal.Profil;

public interface ProfilService {

	List<Profil> get();

	Profil get(Long id);

	void save(Profil profil);

	void delete(Long id);
}