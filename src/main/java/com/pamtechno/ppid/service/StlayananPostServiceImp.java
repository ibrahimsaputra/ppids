package com.pamtechno.ppid.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pamtechno.ppid.modal.Menu;
import com.pamtechno.ppid.modal.StlayananPost;

import com.pamtechno.ppid.dao.StlayananPostDAO;

@Service
public class StlayananPostServiceImp implements StlayananPostService {

	@Autowired
	private StlayananPostDAO stlayananPostDao;
	@Autowired
	private FileStorageService storageService;
	@Autowired
	private MenuService menuService;

	@Transactional
	@Override
	public List<StlayananPost> get(){
		return stlayananPostDao.get();
	}

	@Transactional
	@Override
	public StlayananPost get(Long id){
		return stlayananPostDao.get(id);
	}

	@Transactional
	@Override
	public void save(StlayananPost stlayananPost,Long id_menu){
		Menu menu = menuService.get(id_menu);
		if (stlayananPost.getId() != null && stlayananPost.getId().equals(0L)) {
			StlayananPost oldPost = stlayananPostDao.get(stlayananPost.getId());
			//Delete oldFile if new one exist
			if (stlayananPost.getThumbnail() != null && !stlayananPost.getThumbnail().isEmpty()){
				String linkUrl = oldPost.getThumbnail();
				String[] linkUrls = linkUrl.split("/");
			    storageService.deleteIfExist(linkUrls[linkUrls.length-1]);
			}
		}
		if (menu != null) {
			stlayananPost.setMenu(menu);
			menu.getStLayanans().add(stlayananPost);
			stlayananPostDao.save(stlayananPost);
			
		}
	}

	@Transactional
	@Override
	public void delete(Long id){
		stlayananPostDao.delete(id);
	}

}