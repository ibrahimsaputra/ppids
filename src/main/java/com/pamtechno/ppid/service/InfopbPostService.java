package com.pamtechno.ppid.service;

import java.util.List;
import java.util.Map;

import com.pamtechno.ppid.modal.InfopbPost;

public interface InfopbPostService {

	List<Map<String, Object>> get();

	Map<String, Object> get(String id);

	List<Map<String, Object>> getByIdMenu(String id_menu);

	String save(Map<String, Object> infopbPost, String id_menu);

	String update(Map<String, Object> infopbPost, String id_menu, String id);

	void delete(String id);
}