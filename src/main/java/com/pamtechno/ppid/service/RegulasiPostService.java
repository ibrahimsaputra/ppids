package com.pamtechno.ppid.service;

import java.util.List;
import com.pamtechno.ppid.modal.RegulasiPost;

public interface RegulasiPostService {

	List<RegulasiPost> get();

	RegulasiPost get(Long id);

	void save(RegulasiPost regulasiPost,Long id_menu);

	void delete(Long id);
}