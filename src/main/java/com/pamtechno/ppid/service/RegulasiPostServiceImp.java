package com.pamtechno.ppid.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pamtechno.ppid.modal.Menu;
import com.pamtechno.ppid.modal.RegulasiPost;

import com.pamtechno.ppid.dao.RegulasiPostDAO;

@Service
public class RegulasiPostServiceImp implements RegulasiPostService {

	@Autowired
	private RegulasiPostDAO regulasiPostDao;
	@Autowired
	private MenuService menuService;
	@Autowired
	private FileStorageService storageService;

	@Transactional
	@Override
	public List<RegulasiPost> get(){
		return regulasiPostDao.get();
	}

	@Transactional
	@Override
	public RegulasiPost get(Long id){
		return regulasiPostDao.get(id);
	}

	@Transactional
	@Override
	public void save(RegulasiPost regulasiPost,Long id_menu){
		Menu menu = menuService.get(id_menu);
		
		if (regulasiPost.getId() != null && regulasiPost.getId().equals(0L)) {
			RegulasiPost oldPost = regulasiPostDao.get(regulasiPost.getId());
			//Delete oldFile if new one exist
			if (regulasiPost.getThumbnail() != null && !regulasiPost.getThumbnail().isEmpty()){
				String linkUrl = oldPost.getThumbnail();
				String[] linkUrls = linkUrl.split("/");
			    storageService.deleteIfExist(linkUrls[linkUrls.length-1]);
			}
		}
		
		if (menu != null) {
			regulasiPost.setMenu(menu);
			menu.getRegulasis().add(regulasiPost);
			regulasiPostDao.save(regulasiPost);
			
		}
	}

	@Transactional
	@Override
	public void delete(Long id){
		regulasiPostDao.delete(id);
	}

}