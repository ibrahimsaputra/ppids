package com.pamtechno.ppid.service;

import java.util.List;
import com.pamtechno.ppid.modal.ModulAkses;

public interface ModulAksesService {

	List<ModulAkses> get();

//	ModulAkses get(Long id);

	void save(ModulAkses modulAkses);

//	void delete(Long id);
}