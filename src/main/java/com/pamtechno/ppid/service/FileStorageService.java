package com.pamtechno.ppid.service;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {
	
	public void init();
	
	public static String blank = "";
	public String save(MultipartFile file, String prefix, String postfix);
	public default String save(MultipartFile file) {
		return save(file,blank,blank);
	}
	
	public Resource load(String filename);
	
	public void deleteAll();
	
	public void deleteIfExist(String filename);
	
	public Stream<Path> loadAll();
}
