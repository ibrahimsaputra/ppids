package com.pamtechno.ppid.service;

import java.util.List;
import com.pamtechno.ppid.modal.Permohonan;

public interface PermohonanService {

	List<Permohonan> get();

	Permohonan get(Long id);

	void save(Permohonan permohonan);

	void delete(Long id);
}