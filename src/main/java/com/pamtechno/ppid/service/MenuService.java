package com.pamtechno.ppid.service;

import java.util.List;
import com.pamtechno.ppid.modal.Menu;

public interface MenuService {

	List<Menu> get();

	Menu get(Long id);

	void save(Menu menu, Long id_parent);

	void delete(Long id);
}