package com.pamtechno.ppid.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pamtechno.ppid.modal.Modul;

import com.pamtechno.ppid.dao.ModulDAO;

@Service
public class ModulServiceImp implements ModulService {

	@Autowired
	private ModulDAO modulDao;

	@Transactional
	@Override
	public List<Modul> get(){
		return modulDao.get();
	}

	@Transactional
	@Override
	public Modul get(Long id){
		return modulDao.get(id);
	}

	@Transactional
	@Override
	public void save(Modul modul){
		modulDao.save(modul);
	}

	@Transactional
	@Override
	public void delete(Long id){
		modulDao.delete(id);
	}

}