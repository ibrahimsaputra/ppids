package com.pamtechno.ppid.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileStorageServiceImp implements FileStorageService {
	
	private final Path root = Paths.get("uploads");
	
	@Override
	public void init() {
		
		try {
			Files.createDirectory(root);
		}catch (IOException e) {
			throw new RuntimeException("Could not initialize folder for upload!");
		}
		
	}
	@Value("${app.file.cdn}")
	private String fileCDN;
	@Override
	public String save(MultipartFile file, String prefix, String postfix) {
		
		String url = null;
		if (file != null) {
			//TO-DO change fileName
			if (!prefix.isEmpty()) prefix +="_";
			if (!postfix.isEmpty()) postfix ="_"+postfix;
			
			String filename = StringUtils.cleanPath(file.getOriginalFilename());
			filename = filename.toLowerCase().replaceAll(" ", "_");
			String extension = filename.substring((filename.lastIndexOf(".")));
			filename = prefix+filename.substring(0,filename.lastIndexOf("."))+postfix;
			url = fileCDN+filename;
			filename+=extension;
			try {
				Files.copy(file.getInputStream(), this.root.resolve(filename),StandardCopyOption.REPLACE_EXISTING);
			}catch (Exception e) {
				// TODO: handle exception
				throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
			}
		}
		
		return url;
	}
	
	@Override
	public Resource load(String filename) {
		
		try {
			Path file = root.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			
			if (resource.exists() || resource.isReadable()) {
				return resource;
			}else{
				throw new RuntimeException("Could not read the file!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("Error:" + e.getMessage());
		}
		
	}
	
	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(root.toFile());
	}
	
	@Override
	public void deleteIfExist(String fileName) {
		try {
			Files.deleteIfExists(this.root.resolve(fileName));
		} catch(NoSuchFileException e) 
        { 
            System.out.println("No such file/directory exists"); 
        } 
        catch(DirectoryNotEmptyException e) 
        { 
            System.out.println("Directory is not empty."); 
        } 
        catch(IOException e) 
        { 
            System.out.println("Invalid permissions."); 
        } 
          
        System.out.println("Deletion successful."); 
	}
	
	@Override
	public Stream<Path> loadAll(){
		try {
			return Files.walk(this.root, 1).filter(path-> !path.equals(this.root)).map(this.root::relativize);
		} catch (Exception e) {
			throw new RuntimeException("Could not load the files!");
		}
	}
}
