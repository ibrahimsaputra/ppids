package com.pamtechno.ppid.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pamtechno.ppid.modal.ModulAkses;

import com.pamtechno.ppid.dao.ModulAksesDAO;

@Service
public class ModulAksesServiceImp implements ModulAksesService {

	@Autowired
	private ModulAksesDAO modulAksesDao;

	@Transactional
	@Override
	public List<ModulAkses> get(){
		return modulAksesDao.get();
	}

//	@Transactional
//	@Override
//	public ModulAkses get(Long id){
//		return modulAksesDao.get(id);
//	}

	@Transactional
	@Override
	public void save(ModulAkses modulAkses){
		modulAksesDao.save(modulAkses);
	}

//	@Transactional
//	@Override
//	public void delete(Long id){
//		modulAksesDao.delete(id);
//	}

}