package com.pamtechno.ppid;

import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.pamtechno.ppid.service.FileStorageService;

@SpringBootApplication
public class PPIDApplication implements CommandLineRunner{
	@Resource
	FileStorageService storageService;
	
	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Jakarta"));
	}

	public static void main(String[] args) {
		SpringApplication.run(PPIDApplication.class, args);
	}
	
	@Override
	public void run(String... arg) throws Exception{
		storageService.deleteAll();
		storageService.init();
	}
	
}
