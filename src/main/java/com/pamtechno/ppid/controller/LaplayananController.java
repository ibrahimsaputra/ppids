package com.pamtechno.ppid.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.pamtechno.ppid.helper.ValidatorHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.pamtechno.ppid.modal.Laplayanan;
import com.pamtechno.ppid.service.FileStorageService;
import com.pamtechno.ppid.service.LaplayananService;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api")
public class LaplayananController {

	@Autowired
	private LaplayananService laplayananService;
	@Autowired
	private FileStorageService storageService;

	private Logger log = Logger.getLogger(getClass());

	@GetMapping("/laplayanan")
	public List<Map<String, Object>> get(){
		return laplayananService.get();
	}

	@GetMapping("/laplayanan/{id}")
	public Map<String, Object> get(@PathVariable Long id){
		return laplayananService.get(id);
	}

	@PostMapping("/laplayanan")
	public Object save(@RequestParam Map<String, Object> request,
					   @RequestParam("link") MultipartFile link){
		Map<String, Object> validation = new ValidatorHelper()
				.check("tahun","required")
				.check("enabled","required")
				.validate(request);

		if (validation.size() == 0){
			Date now = new Date();
			request.put("link",storageService.save(link,"laplayanan",Long.toString(now.getTime())));
			laplayananService.save(request);
			return request;
		}else{
			return "Data tidak lengkap";
		}
	}

	@PutMapping("/laplayanan/{id}")
	public Object update(@RequestParam Map<String, Object> request,
						 @RequestParam("link") MultipartFile link,
						 @PathVariable String id){
		Map<String, Object> validation = new ValidatorHelper()
				.check("tahun","required")
				.check("enabled","required")
				.validate(request);

		if (validation.size() == 0){
			Date now = new Date();
			request.put("link",storageService.save(link,"laplayanan",Long.toString(now.getTime())));
			laplayananService.update(request, id);
			return request;
		}else{
			return "Data tidak lengkap";
		}
	}

	@DeleteMapping("/laplayanan/{id}")
	public String delete(@PathVariable String id){
		laplayananService.delete(id);
		return "Dip removed with id "+id;
	}

}