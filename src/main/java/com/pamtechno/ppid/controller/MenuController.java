package com.pamtechno.ppid.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pamtechno.ppid.modal.Menu;
import com.pamtechno.ppid.service.MenuService;

@RestController
@RequestMapping("/api")
public class MenuController {

	@Autowired
	private MenuService menuService;

	@GetMapping("/menu")
	public List<Menu> get(){
		return menuService.get();
	}

	@GetMapping("/menu/{id}")
	public Menu get(@PathVariable Long id){
		return menuService.get(id);
	}

	@PostMapping("/menu")
	public Menu save(
			@RequestParam("id_parent") Long id_parent,
			@RequestParam("nama_menu") String nama_menu,
			@RequestParam("tipe") String tipe,
			@RequestParam(value = "link_icon",required = false) MultipartFile link_icon,
			int dump){
		Menu menu = new Menu();
		menu.setNama_menu(nama_menu);
		menu.setTipe(tipe);
		menuService.save(menu,id_parent);
		return menu;
	}

	@PutMapping("/menu/{id}")
	public Menu update(
			@RequestParam("id_parent") Long id_parent,
			@RequestParam("nama_menu") String nama_menu,
			@RequestParam("tipe") String tipe,
			@RequestParam("link_icon") String link_icon,
			@PathVariable("id") Long id){
		Menu menu = new Menu();
		menu.setId(id);
		menu.setNama_menu(nama_menu);
		menu.setTipe(tipe);
		menuService.save(menu,id_parent);
		return menu;
	}

	@DeleteMapping("/menu/{id}")
	public String delete(@PathVariable Long id){
		menuService.delete(id);
		return "Menu removed with id "+id;
	}

}