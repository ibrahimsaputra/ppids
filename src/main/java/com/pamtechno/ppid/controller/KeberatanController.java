package com.pamtechno.ppid.controller;

import com.pamtechno.ppid.helper.ValidatorHelper;
import com.pamtechno.ppid.service.KeberatanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class KeberatanController {

	@Autowired
	private KeberatanService keberatanService;

	@GetMapping("/keberatan")
	public List<Map<String, Object>> get(){
		return keberatanService.get();
	}

	@GetMapping("/keberatan/{id}")
	public Map<String, Object> get(@PathVariable String id){
		return keberatanService.get(id);
	}

	@PostMapping("/keberatan")
	public Object save(@RequestBody Map<String, Object> request){

		Map<String, Object> validation = new ValidatorHelper()
				.check("nama_pemohon", "required")
				.check("email", "required")
				.check("alamat", "required")
				.check("nohp", "required")
				.check("pil_keberatan", "required")
				.check("als_keberatan", "required")
				.check("status", "required")
				.validate(request);

		if (validation.size() == 0){
			String result = keberatanService.save(request);
			if (!result.equals("")){
				return result;
			}else {
				return request;
			}
		}else{
			return "Data tidak lengkap";
		}

	}

	@PutMapping("/keberatan/{id}")
	public Object update(@RequestBody  Map<String, Object> request, @PathVariable("id") String id){
		Map<String, Object> validation = new ValidatorHelper()
				.check("nama_pemohon", "required")
				.check("email", "required")
				.check("alamat", "required")
				.check("nohp", "required")
				.check("pil_keberatan", "required")
				.check("als_keberatan", "required")
				.check("status", "required")
				.validate(request);

		if (validation.size() == 0){
			String result = keberatanService.update(request, id);
			if (!result.equals("")){
				return result;
			}else {
				return request;
			}
		}else{
			return "Data tidak lengkap";
		}
	}

	@DeleteMapping("/keberatan/{id}")
	public String delete(@PathVariable String id){
		keberatanService.delete(id);
		return "Keberatan removed with id "+id;
	}

}