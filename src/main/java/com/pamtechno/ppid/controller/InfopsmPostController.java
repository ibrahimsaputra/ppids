package com.pamtechno.ppid.controller;

import com.pamtechno.ppid.helper.ValidatorHelper;
import com.pamtechno.ppid.service.FileStorageService;
import com.pamtechno.ppid.service.InfopsmPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class InfopsmPostController {

	@Autowired
	private InfopsmPostService infopsmPostService;

	@Autowired
	private FileStorageService storageService;

	@GetMapping("/infopsmpost")
	public List<Map<String, Object>> get(){
		return infopsmPostService.get();
	}

	@GetMapping("/infopsmpost/{id}")
	public Object get(@PathVariable String id){
		return infopsmPostService.get(id);
	}

	@GetMapping("/infopsmpost/bymenu/{id}")
	public List<Map<String, Object>> getByIdMenu(@PathVariable String id_menu){
		return infopsmPostService.getByIdMenu(id_menu);
	}

	@PostMapping("/infopsmpost")
	public Object save(
			@RequestParam Map<String, Object> request,
			@RequestParam(value = "thumbnail",required = false) MultipartFile thumbnail,
			@RequestParam(value = "link_pdf", required = false) MultipartFile link_pdf
	){
		Map<String, Object> validation = new ValidatorHelper()
				.check("id_menu", "required")
				.check("judul", "required")
				.check("body", "required")
				.validate(request);

		if (validation.size() == 0){
			Date now = new Date();
			request.put("thumbnail",storageService.save(thumbnail,"infopsmPost",Long.toString(now.getTime())));
			request.put("link_pdf",storageService.save(link_pdf,"infopsmPost",Long.toString(now.getTime())));

			String result = infopsmPostService.save(request, request.get("id_menu").toString());
			if (!result.equals("")){
				return result;
			}else {
				return request;
			}
		}else{
			return "Data tidak lengkap";
		}
	}

	@PutMapping("/infopsmpost/{id}")
	public Object update(
			@RequestParam Map<String, Object> request,
			@RequestParam(value = "thumbnail",required = false) MultipartFile thumbnail,
			@RequestParam(value = "link_pdf", required = false) MultipartFile link_pdf,
			@PathVariable("id") String id){
		Map<String, Object> validation = new ValidatorHelper()
				.check("id_menu", "required")
				.check("judul", "required")
				.check("body", "required")
				.validate(request);

		if (validation.size() == 0){Date now = new Date();
			request.put("thumbnail",storageService.save(thumbnail,"infopsmPost",Long.toString(now.getTime())));
			request.put("link_pdf",storageService.save(link_pdf,"infopsmPost",Long.toString(now.getTime())));

			String result = infopsmPostService.update(request, request.get("id_menu").toString(), id);
			if (!result.equals("")){
				return result;
			}else {
				return request;
			}
		}else{
			return "Data tidak lengkap";
		}
	}

	@DeleteMapping("/infopsmpost/{id}")
	public String delete(@PathVariable String id){
		infopsmPostService.delete(id);
		return "InfopsmPost removed with id "+id;
	}

}