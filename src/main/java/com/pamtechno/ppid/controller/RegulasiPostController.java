package com.pamtechno.ppid.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pamtechno.ppid.modal.RegulasiPost;
import com.pamtechno.ppid.service.FileStorageService;
import com.pamtechno.ppid.service.RegulasiPostService;

@RestController
@RequestMapping("/api")
public class RegulasiPostController {

	@Autowired
	private RegulasiPostService regulasiPostService;
	
	@Autowired
	private FileStorageService storageService;

	@GetMapping("/regulasipost")
	public List<RegulasiPost> get(){
		return regulasiPostService.get();
	}

	@GetMapping("/regulasipost/{id}")
	public RegulasiPost get(@PathVariable Long id){
		return regulasiPostService.get(id);
	}

	@PostMapping("/regulasipost")
	public RegulasiPost save(
			@RequestParam("id_menu") Long id_menu,
			@RequestParam("judul") String judul,
			@RequestParam("body") String body,
			@RequestParam(value="thumbnail",required = false) MultipartFile thumbnail,
			int dump){
		RegulasiPost regulasiPost = new RegulasiPost();
		regulasiPost.setJudul(judul);
		regulasiPost.setBody(body);
		Date now = new Date();
		regulasiPost.setThumbnail(storageService.save(thumbnail,"regulasiPost",Long.toString(now.getTime())));
		
		regulasiPostService.save(regulasiPost,id_menu);
		return regulasiPost;
	}

	@PutMapping("/regulasipost/{id}")
	public RegulasiPost update(
			@RequestParam("id_menu") Long id_menu,
			@RequestParam("judul") String judul,
			@RequestParam("body") String body,
			@RequestParam(value="thumbnail",required = false) MultipartFile thumbnail,
			@PathVariable("id") Long id){
		RegulasiPost regulasiPost = new RegulasiPost();
		regulasiPost.setId(id);
		regulasiPost.setJudul(judul);
		regulasiPost.setBody(body);
		Date now = new Date();
		regulasiPost.setThumbnail(storageService.save(thumbnail,"regulasiPost",Long.toString(now.getTime())));
		regulasiPostService.save(regulasiPost,id_menu);
		return regulasiPost;
	}

	@DeleteMapping("/regulasipost/{id}")
	public String delete(@PathVariable Long id){
		regulasiPostService.delete(id);
		return "RegulasiPost removed with id "+id;
	}

}