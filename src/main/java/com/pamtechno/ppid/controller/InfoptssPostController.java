package com.pamtechno.ppid.controller;

import com.pamtechno.ppid.helper.ValidatorHelper;
import com.pamtechno.ppid.service.FileStorageService;
import com.pamtechno.ppid.service.InfoptssPostService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class InfoptssPostController {

	@Autowired
	private InfoptssPostService infoptssPostService;

	@Autowired
	private FileStorageService storageService;

	private Logger log = Logger.getLogger(getClass());

	@GetMapping("/infoptsspost")
	public List<Map<String, Object>> get(){
		return infoptssPostService.get();
	}

	@GetMapping("/infoptsspost/{id}")
	public Object get(@PathVariable String id){
		return infoptssPostService.get(id);
	}

	@GetMapping("/infoptsspost/bymenu/{id}")
	public List<Map<String, Object>> getByIdMenu(@PathVariable String id_menu){
		return infoptssPostService.getByIdMenu(id_menu);
	}

	@PostMapping("/infoptsspost")
	public Object save(
			@RequestParam Map<String, Object> request,
			@RequestParam(name = "thumbnail",required = false) MultipartFile thumbnail,
			@RequestParam(name = "link_pdf", required = false) MultipartFile link_pdf){
		Map<String, Object> validation = new ValidatorHelper()
				.check("id_menu", "required")
				.check("judul", "required")
				.check("body", "required")
				.validate(request);

		if (validation.size() == 0){
			Date now = new Date();
			request.put("thumbnail",storageService.save(thumbnail,"infoptpsPost",Long.toString(now.getTime())));
			request.put("link_pdf",storageService.save(link_pdf,"infoptpsPost",Long.toString(now.getTime())));

			String result = infoptssPostService.save(request, request.get("id_menu").toString());
			if (!result.equals("")){
				return result;
			}else {
				return request;
			}
		}else{
			return "Data tidak lengkap";
		}
	}

	@PutMapping("/infoptsspost/{id}")
	public Object update(
			@RequestParam Map<String, Object> request,
			@RequestParam(name = "thumbnail",required = false) MultipartFile thumbnail,
			@RequestParam(name = "link_pdf", required = false) MultipartFile link_pdf,
			@PathVariable("id") String id){
		Map<String, Object> validation = new ValidatorHelper()
				.check("id_menu", "required")
				.check("judul", "required")
				.check("body", "required")
				.validate(request);

		if (validation.size() == 0){
			Date now = new Date();
			request.put("thumbnail",storageService.save(thumbnail,"infoptpsPost",Long.toString(now.getTime())));
			request.put("link_pdf",storageService.save(link_pdf,"infoptpsPost",Long.toString(now.getTime())));
			String result = infoptssPostService.update(request, request.get("id_menu").toString(), id);
			if (!result.equals("")){
				return result;
			}else {
				return request;
			}
		}else{
			return "Data tidak lengkap";
		}
	}

	@DeleteMapping("/infoptsspost/{id}")
	public String delete(@PathVariable String id){
		infoptssPostService.delete(id);
		return "InfopsmPost removed with id "+id;
	}

}