package com.pamtechno.ppid.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pamtechno.ppid.modal.ModulAkses;
import com.pamtechno.ppid.service.ModulAksesService;

@RestController
@RequestMapping("/api")
public class ModulAksesController {

	@Autowired
	private ModulAksesService modulAksesService;

	@GetMapping("/modulakses")
	public List<ModulAkses> get(){
		return modulAksesService.get();
	}



	@PostMapping("/modulakses")
	public ModulAkses save(
			@RequestParam("id_level") Long id_level,
			@RequestParam("id_modul") Long id_modul,
			@RequestParam("l") Boolean l,
			@RequestParam("d") Boolean d,
			@RequestParam("t") Boolean t,
			@RequestParam("u") Boolean u,
			@RequestParam("h") Boolean h,
			int dump){
		ModulAkses modulAkses = new ModulAkses();
//		modulAkses.setId_level(id_level);
//		modulAkses.setId_modul(id_modul);
		modulAkses.setL(l);
		modulAkses.setD(d);
		modulAkses.setT(t);
		modulAkses.setU(u);
		modulAkses.setH(h);
		modulAksesService.save(modulAkses);
		return modulAkses;
	}

	@PutMapping("/modulakses/{id}")
	public ModulAkses update(
			@RequestParam("id_level") Long id_level,
			@RequestParam("id_modul") Long id_modul,
			@RequestParam("l") Boolean l,
			@RequestParam("d") Boolean d,
			@RequestParam("t") Boolean t,
			@RequestParam("u") Boolean u,
			@RequestParam("h") Boolean h,
			@PathVariable("id") Long id){
		ModulAkses modulAkses = new ModulAkses();
//		modulAkses.setId_level(id_level);
//		modulAkses.setId_modul(id_modul);
		modulAkses.setL(l);
		modulAkses.setD(d);
		modulAkses.setT(t);
		modulAkses.setU(u);
		modulAkses.setH(h);
		modulAksesService.save(modulAkses);
		return modulAkses;
	}

	@DeleteMapping("/modulakses/{id}")
	public String delete(@PathVariable Long id){
//		modulAksesService.delete(id);
		return "ModulAkses removed with id "+id;
	}

}