package com.pamtechno.ppid.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pamtechno.ppid.modal.UserLevel;
import com.pamtechno.ppid.service.UserLevelService;

@RestController
@RequestMapping("/api")
public class UserLevelController {

	@Autowired
	private UserLevelService userLevelService;

	@GetMapping("/userlevel")
	public List<UserLevel> get(){
		return userLevelService.get();
	}

	@GetMapping("/userlevel/{id}")
	public UserLevel get(@PathVariable Long id){
		return userLevelService.get(id);
	}

	@PostMapping("/userlevel")
	public UserLevel save(
			@RequestParam("level_name") String level_name,
			int dump){
		UserLevel userLevel = new UserLevel();
		userLevel.setLevel_name(level_name);
		userLevelService.save(userLevel);
		return userLevel;
	}

	@PutMapping("/userlevel/{id}")
	public UserLevel update(
			@RequestParam("level_name") String level_name,
			@PathVariable("id") Long id){
		UserLevel userLevel = new UserLevel();
		userLevel.setId(id);
		userLevel.setLevel_name(level_name);
		userLevelService.save(userLevel);
		return userLevel;
	}

	@DeleteMapping("/userlevel/{id}")
	public String delete(@PathVariable Long id){
		userLevelService.delete(id);
		return "UserLevel removed with id "+id;
	}

}