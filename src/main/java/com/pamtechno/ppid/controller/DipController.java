package com.pamtechno.ppid.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pamtechno.ppid.helper.ValidatorHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.pamtechno.ppid.modal.Dip;
import com.pamtechno.ppid.service.DipService;

@RestController
@RequestMapping("/api")
public class DipController {

	@Autowired
	private DipService dipService;

	private Logger log = Logger.getLogger(getClass());

	@GetMapping("/dip")
	public List<Map<String, Object>> get(){
		return dipService.get();
	}

	@GetMapping("/dip/{id}")
	public Map<String, Object> get(@PathVariable Long id){
		return dipService.get(id);
	}

	@PostMapping("/dip")
	public Object save(@RequestBody Map<String, Object> request){
		Map<String, Object> validation = new ValidatorHelper()
				.check("rincian","required")
				.check("pejabat","required")
				.check("pngjwb","required")
				.check("wktbuatinfo","required")
				.check("tmptbuatinfo","required")
				.check("bfi_soft","required")
				.check("bfi_hard","required")
				.check("ji_bk","required")
				.check("ji_ts","required")
				.check("ji_sm","required")
				.check("jk_wkt_simpan","required")
				.check("keterangan","required")
				.validate(request);

		if (validation.size() == 0){
			dipService.save(request);
			return request;
		}else{
			return "Data tidak lengkap";
		}
	}

	@PutMapping("/dip/{id}")
	public Object update(@RequestBody Map<String, Object> request, @PathVariable String id){
		Map<String, Object> validation = new ValidatorHelper()
				.check("rincian","required")
				.check("pejabat","required")
				.check("pngjwb","required")
				.check("wktbuatinfo","required")
				.check("tmptbuatinfo","required")
				.check("bfi_soft","required")
				.check("bfi_hard","required")
				.check("ji_bk","required")
				.check("ji_ts","required")
				.check("ji_sm","required")
				.check("jk_wkt_simpan","required")
				.check("keterangan","required")
				.validate(request);

		if (validation.size() == 0){
			dipService.update(request, id);
			return request;
		}else{
			return "Data tidak lengkap";
		}
	}

	@DeleteMapping("/dip/{id}")
	public String delete(@PathVariable String id){
		dipService.delete(id);
		return "Dip removed with id "+id;
	}

}