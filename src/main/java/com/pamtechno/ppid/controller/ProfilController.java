package com.pamtechno.ppid.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pamtechno.ppid.modal.Profil;
import com.pamtechno.ppid.service.FileStorageService;
import com.pamtechno.ppid.service.ProfilService;

@RestController
@RequestMapping("/api")
public class ProfilController {

	@Autowired
	private ProfilService profilService;
	
	@Autowired
	private FileStorageService storageService;

	@GetMapping("/profil")
	public List<Profil> get(){
		return profilService.get();
	}

	@GetMapping("/profil/{id}")
	public Profil get(@PathVariable Long id){
		return profilService.get(id);
	}

	@PostMapping("/profil")
	public Profil save(
			@RequestParam("profil_singkat") String profil_singkat,
			@RequestParam("tugas_fungsi") String tugas_fungsi,
			@RequestParam("link_struktur") MultipartFile link_struktur,
			@RequestParam("visi_misi") String visi_misi,
			int dump){
		Profil profil = new Profil();
		profil.setProfil_singkat(profil_singkat);
		profil.setTugas_fungsi(tugas_fungsi);
		Date now = new Date();
		profil.setLink_struktur(storageService.save(link_struktur,"profil",Long.toString(now.getTime())));
		profil.setVisi_misi(visi_misi);
		profilService.save(profil);
		return profil;
	}

	@PutMapping("/profil/{id}")
	public Profil update(
			@RequestParam("profil_singkat") String profil_singkat,
			@RequestParam("tugas_fungsi") String tugas_fungsi,
			@RequestParam(name="link_struktur",required = false) MultipartFile link_struktur,
			@RequestParam("visi_misi") String visi_misi,
			@PathVariable("id") Long id){
		Profil profil = new Profil();
		profil.setId(id);
		profil.setProfil_singkat(profil_singkat);
		profil.setTugas_fungsi(tugas_fungsi);
		Date now = new Date();
		profil.setLink_struktur(storageService.save(link_struktur,"profil",Long.toString(now.getTime())));
		profil.setVisi_misi(visi_misi);
		profilService.save(profil);
		return profil;
	}

	@DeleteMapping("/profil/{id}")
	public String delete(@PathVariable Long id){
		profilService.delete(id);
		return "Profil removed with id "+id;
	}

}