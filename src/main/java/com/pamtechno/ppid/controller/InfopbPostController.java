package com.pamtechno.ppid.controller;

import com.pamtechno.ppid.helper.ValidatorHelper;
import com.pamtechno.ppid.service.FileStorageService;
import com.pamtechno.ppid.service.InfopbPostService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class InfopbPostController {

	@Autowired
	private InfopbPostService infopbPostService;

	private Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private FileStorageService storageService;

	@GetMapping("/infopbpost")
	public List<Map<String, Object>> get(){
		return infopbPostService.get();
	}

	@GetMapping("/infopbpost/{id}")
	public Object get(@PathVariable String id){
		return infopbPostService.get(id);
	}

	@GetMapping("/infopbpost/bymenu/{id}")
	public List<Map<String, Object>> getByIdMenu(@PathVariable String id_menu){
		return infopbPostService.getByIdMenu(id_menu);
	}

	@PostMapping("/infopbpost")
	public Object save(
		@RequestParam Map<String, Object> request,
	   	@RequestParam(value = "thumbnail",required = false) MultipartFile thumbnail,
	   	@RequestParam(value = "link_pdf", required = false) MultipartFile link_pdf
	){
		Map<String, Object> validation = new ValidatorHelper()
				.check("id_menu", "required")
				.check("judul", "required")
				.check("body", "required")
				.validate(request);

		Date now = new Date();
		request.put("thumbnail",storageService.save(thumbnail,"infopbPost",Long.toString(now.getTime())));
		request.put("link_pdf",storageService.save(link_pdf,"infopbPost",Long.toString(now.getTime())));

		if (validation.size() == 0){
			String result = infopbPostService.save(request, request.get("id_menu").toString());
			if (!result.equals("")){
				return result;
			}else {
				return request;
			}
		}else{
			return "Data tidak lengkap";
		}
	}

	@PutMapping("/infopbpost/{id}")
	public Object update(
			@RequestParam Map<String, Object> request,
			@RequestParam(value = "thumbnail",required = false) MultipartFile thumbnail,
			@RequestParam(value = "link_pdf", required = false) MultipartFile link_pdf,
			@PathVariable("id") String id){
		Map<String, Object> validation = new ValidatorHelper()
				.check("id_menu", "required")
				.check("judul", "required")
				.check("body", "required")
				.validate(request);

		Date now = new Date();
		request.put("thumbnail",storageService.save(thumbnail,"infopbPost",Long.toString(now.getTime())));
		request.put("link_pdf",storageService.save(link_pdf,"infopbPost",Long.toString(now.getTime())));

		if (validation.size() == 0){
			String result = infopbPostService.update(request, request.get("id_menu").toString(), id);
			if (!result.equals("")){
				return result;
			}else {
				return request;
			}
		}else{
			return "Data tidak lengkap";
		}

	}

	@DeleteMapping("/infopbpost/{id}")
	public String delete(@PathVariable String id){
		infopbPostService.delete(id);
		return "InfopbPost removed with id "+id;
	}
}