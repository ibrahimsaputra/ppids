package com.pamtechno.ppid.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pamtechno.ppid.modal.User;
import com.pamtechno.ppid.service.UserService;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("/user")
	public List<User> get(){
		return userService.get();
	}

	@GetMapping("/user/{id}")
	public User get(@PathVariable Long id){
		return userService.get(id);
	}

	@PostMapping("/user")
	public User save(
			@RequestParam("id_level") Long id_level,
			@RequestParam("username") String username,
			@RequestParam("password") String password,
			int dump){
		User user = new User();
		user.setId_level(id_level);
		user.setUsername(username);
		user.setPassword(password);
		userService.save(user);
		return user;
	}

	@PutMapping("/user/{id}")
	public User update(
			@RequestParam("id_level") Long id_level,
			@RequestParam("username") String username,
			@RequestParam("password") String password,
			@PathVariable("id") Long id){
		User user = new User();
		user.setId(id);
		user.setId_level(id_level);
		user.setUsername(username);
		user.setPassword(password);
		userService.save(user);
		return user;
	}

	@DeleteMapping("/user/{id}")
	public String delete(@PathVariable Long id){
		userService.delete(id);
		return "User removed with id "+id;
	}

}