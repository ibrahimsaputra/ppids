package com.pamtechno.ppid.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pamtechno.ppid.modal.Modul;
import com.pamtechno.ppid.service.ModulService;

@RestController
@RequestMapping("/api")
public class ModulController {

	@Autowired
	private ModulService modulService;

	@GetMapping("/modul")
	public List<Modul> get(){
		return modulService.get();
	}

	@GetMapping("/modul/{id}")
	public Modul get(@PathVariable Long id){
		return modulService.get(id);
	}

	@PostMapping("/modul")
	public Modul save(
			@RequestParam("kode") String kode,
			@RequestParam("nama") String nama,
			int dump){
		Modul modul = new Modul();
		modul.setKode(kode);
		modul.setNama(nama);
		modulService.save(modul);
		return modul;
	}

	@PutMapping("/modul/{id}")
	public Modul update(
			@RequestParam("kode") String kode,
			@RequestParam("nama") String nama,
			@PathVariable("id") Long id){
		Modul modul = new Modul();
		modul.setId(id);
		modul.setKode(kode);
		modul.setNama(nama);
		modulService.save(modul);
		return modul;
	}

	@DeleteMapping("/modul/{id}")
	public String delete(@PathVariable Long id){
		modulService.delete(id);
		return "Modul removed with id "+id;
	}

}