package com.pamtechno.ppid.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pamtechno.ppid.modal.StlayananPost;
import com.pamtechno.ppid.service.FileStorageService;
import com.pamtechno.ppid.service.StlayananPostService;

@RestController
@RequestMapping("/api")
public class StlayananPostController {

	@Autowired
	private StlayananPostService stlayananPostService;
	@Autowired
	private FileStorageService storageService;

	@GetMapping("/stlayananpost")
	public List<StlayananPost> get(){
		return stlayananPostService.get();
	}

	@GetMapping("/stlayananpost/{id}")
	public StlayananPost get(@PathVariable Long id){
		return stlayananPostService.get(id);
	}

	@PostMapping("/stlayananpost")
	public StlayananPost save(
			@RequestParam("id_menu") Long id_menu,
			@RequestParam("judul") String judul,
			@RequestParam("body") String body,
			@RequestParam(value="thumbnail",required = false) MultipartFile thumbnail,
			int dump){
		StlayananPost stlayananPost = new StlayananPost();
		stlayananPost.setJudul(judul);
		stlayananPost.setBody(body);
		Date now = new Date();
		stlayananPost.setThumbnail(storageService.save(thumbnail,"stlayananPost",Long.toString(now.getTime())));
		stlayananPostService.save(stlayananPost,id_menu);
		return stlayananPost;
	}

	@PutMapping("/stlayananpost/{id}")
	public StlayananPost update(
			@RequestParam("id_menu") Long id_menu,
			@RequestParam("judul") String judul,
			@RequestParam("body") String body,
			@RequestParam(value="thumbnail",required = false) MultipartFile thumbnail,
			@PathVariable("id") Long id){
		StlayananPost stlayananPost = new StlayananPost();
		stlayananPost.setId(id);
		stlayananPost.setJudul(judul);
		stlayananPost.setBody(body);
		Date now = new Date();
		stlayananPost.setThumbnail(storageService.save(thumbnail,"stlayananPost",Long.toString(now.getTime())));
		stlayananPostService.save(stlayananPost,id_menu);
		return stlayananPost;
	}

	@DeleteMapping("/stlayananpost/{id}")
	public String delete(@PathVariable Long id){
		stlayananPostService.delete(id);
		return "StlayananPost removed with id "+id;
	}

}