package com.pamtechno.ppid.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pamtechno.ppid.modal.Permohonan;
import com.pamtechno.ppid.service.FileStorageService;
import com.pamtechno.ppid.service.PermohonanService;

@RestController
@RequestMapping("/api")
public class PermohonanController {

	@Autowired
	private PermohonanService permohonanService;
	
	@Autowired
	private FileStorageService storageService;

	@GetMapping("/permohonan")
	public List<Permohonan> get(){
		return permohonanService.get();
	}

	@GetMapping("/permohonan/{id}")
	public Permohonan get(@PathVariable Long id){
		return permohonanService.get(id);
	}

	@PostMapping("/permohonan")
	public Permohonan save(
			@RequestParam("nama_pemohon") String nama_pemohon,
			@RequestParam("email") String email,
			@RequestParam("noktp") String noktp,
			@RequestParam("alamat") String alamat,
			@RequestParam("nohp") String nohp,
			@RequestParam("tujuan") String tujuan,
			@RequestParam("tipe_pemohon") String tipe_pemohon,
			@RequestParam("detail_wni") String detail_wni,
			@RequestParam("upload_ktp") MultipartFile upload_ktp,
			@RequestParam("info_kategori") String info_kategori,
			@RequestParam("info_diminta") String info_diminta,
			@RequestParam("cara") String cara,
			@RequestParam("bentuk") String bentuk,
			@RequestParam("alasan") String alasan,
			@RequestParam("kegunaan") String kegunaan,
			@RequestParam("status") String status,
			int dump){
		Permohonan permohonan = new Permohonan();
		permohonan.setNama_pemohon(nama_pemohon);
		permohonan.setEmail(email);
		permohonan.setNoktp(noktp);
		permohonan.setAlamat(alamat);
		permohonan.setNohp(nohp);
		permohonan.setTujuan(tujuan);
		permohonan.setTipe_pemohon(tipe_pemohon);
		permohonan.setDetail_wni(detail_wni);
		Date now = new Date();
		permohonan.setUpload_ktp(storageService.save(upload_ktp,"permohonan",Long.toString(now.getTime())));
		permohonan.setInfo_kategori(info_kategori);
		permohonan.setInfo_diminta(info_diminta);
		permohonan.setCara(cara);
		permohonan.setBentuk(bentuk);
		permohonan.setAlasan(alasan);
		permohonan.setKegunaan(kegunaan);
		permohonan.setStatus(status);
		permohonanService.save(permohonan);
		return permohonan;
	}

	@PutMapping("/permohonan/{id}")
	public Permohonan update(
			@RequestParam("nama_pemohon") String nama_pemohon,
			@RequestParam("email") String email,
			@RequestParam("noktp") String noktp,
			@RequestParam("alamat") String alamat,
			@RequestParam("nohp") String nohp,
			@RequestParam("tujuan") String tujuan,
			@RequestParam("tipe_pemohon") String tipe_pemohon,
			@RequestParam("detail_wni") String detail_wni,
			@RequestParam(name="upload_ktp",required = false) MultipartFile upload_ktp,
			@RequestParam("info_kategori") String info_kategori,
			@RequestParam("info_diminta") String info_diminta,
			@RequestParam("cara") String cara,
			@RequestParam("bentuk") String bentuk,
			@RequestParam("alasan") String alasan,
			@RequestParam("kegunaan") String kegunaan,
			@RequestParam("status") String status,
			@PathVariable("id") Long id){
		Permohonan permohonan = new Permohonan();
		permohonan.setId(id);
		permohonan.setNama_pemohon(nama_pemohon);
		permohonan.setEmail(email);
		permohonan.setNoktp(noktp);
		permohonan.setAlamat(alamat);
		permohonan.setNohp(nohp);
		permohonan.setTujuan(tujuan);
		permohonan.setTipe_pemohon(tipe_pemohon);
		permohonan.setDetail_wni(detail_wni);
		Date now = new Date();
		permohonan.setUpload_ktp(storageService.save(upload_ktp,"permohonan",Long.toString(now.getTime())));
		permohonan.setInfo_kategori(info_kategori);
		permohonan.setInfo_diminta(info_diminta);
		permohonan.setCara(cara);
		permohonan.setBentuk(bentuk);
		permohonan.setAlasan(alasan);
		permohonan.setKegunaan(kegunaan);
		permohonan.setStatus(status);
		permohonanService.save(permohonan);
		return permohonan;
	}

	@DeleteMapping("/permohonan/{id}")
	public String delete(@PathVariable Long id){
		permohonanService.delete(id);
		return "Permohonan removed with id "+id;
	}

}